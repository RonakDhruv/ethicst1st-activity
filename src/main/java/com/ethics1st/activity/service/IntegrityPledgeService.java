package com.ethics1st.activity.service;

/**
 * Method to Call IntegrityPledge
 * This will consume from backEnd application.
 * @author ronakdhruv
 *
 */
public interface IntegrityPledgeService {
	
	/**
	 * 
	 * @param userId
	 */
	public void pledgeSigned(String userId);

}
