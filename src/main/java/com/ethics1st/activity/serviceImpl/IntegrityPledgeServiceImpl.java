package com.ethics1st.activity.serviceImpl;

import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.ethics1st.activity.service.IntegrityPledgeService;

/*
 * Invoke this service from BackEnd code.
 * 
 */
@Service
public class IntegrityPledgeServiceImpl implements IntegrityPledgeService {

	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(IntegrityPledgeServiceImpl.class);

	@Override
	public void pledgeSigned(String userId) {

		LOGGER.info("pledgeSigned method called");

		// Load configuration of activity from activiti.cfg.xml file
		ProcessEngineConfiguration.createProcessEngineConfigurationFromResourceDefault();

		// using Configuration will get Process Engine
		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

		// will get All activity services using processEngine
		// use of Runtime service is to initiate process.
		RuntimeService runtimeService = processEngine.getRuntimeService();

		// To load process diagram in container.
		RepositoryService repositoryService = processEngine.getRepositoryService();

		repositoryService.createDeployment().addClasspathResource("processes/sign-integrity-pledge.bpmn20.xml")
				.deploy();

		// To execute task use task service.
		TaskService taskService = processEngine.getTaskService();

		ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("integrityPledge");

		Task task = taskService.createTaskQuery().processInstanceId(processInstance.getId()).singleResult();
		System.out.print("task is" + task.getName());

		// Completing the phone interview with success should trigger two new tasks
		Map<String, Object> taskVariables = new HashMap<String, Object>();
		taskVariables.put("integrityPledgeAccepted", true);
		
		taskService.setVariable(task.getId(), "integrityPledgeAccepted", true);
		
		System.out.println("value of task variable is"+taskService.getVariable(task.getId(), "integrityPledgeAccepted"));

		taskService.complete(task.getId(), taskVariables);
		
	
	}

}
