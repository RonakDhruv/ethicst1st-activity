package org.activiti.designer.test;

import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Test;

public class ProcessTestIntegrityPledge {

	@Test
	public void startProcess() throws Exception {
	

		ProcessEngineConfiguration.createProcessEngineConfigurationFromResource("activiti.cfg.xml");

		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
		RuntimeService runtimeService = processEngine.getRuntimeService();
		
		RepositoryService repositoryService = processEngine.getRepositoryService();

		repositoryService.createDeployment().addClasspathResource("processes/sign-integrity-pledge.bpmn20.xml").deploy();

		TaskService taskService = processEngine.getTaskService();

//		Map<String, Object> variables = new HashMap<String, Object>();
//		variables.put("applicant", "demo");
		ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("integrityPledge");

		Task task = taskService.createTaskQuery().processInstanceId(processInstance.getId()).singleResult();
		System.out.print("task is" + task.getName());
		
        // Completing the phone interview with success should trigger two new tasks
        Map<String, Object> taskVariables = new HashMap<String, Object>();
        taskVariables.put("integrityPledgeAccepted", true);
        
        taskService.complete(task.getId(), taskVariables);
	}
}